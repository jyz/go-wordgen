package wordgen

import (
	"math/rand"
	"time"
)

var (
	PRE = []string{"bl", "br", "cl", "cr", "dr", "fl",
		"fr", "gl", "gr", "pl", "pr", "sk",
		"sl", "sm", "sn", "sp", "st", "str",
		"sw", "tr", "ch", "sh"}
	POST = []string{"ct", "ft", "mp", "nd", "ng", "nk",
		"nt", "pt", "sk", "sp", "ss", "st", "ch", "sh"}

	VOWELS = []string{"a", "e", "i", "o", "u"}
)

func GenerateWords(length int) (words string) {
	for i := 0; i < length; i++ {
		words += GenerateWord()
	}
	return
}

func GenerateWord() (word string) {
	word = PRE[seedAndReturnRandom(len(PRE))] + VOWELS[seedAndReturnRandom(len(VOWELS))] + POST[seedAndReturnRandom(len(POST))]
	return
}

func GenerateParagraphs(length int) (paragraphs string) {
	for i := 0; i < length; i++ {
		paragraphs += (GenerateWords(seedAndReturnRandom(500)) + "\n\n")
	}
	return
}

// ========= UTIL ========= //

func seedAndReturnRandom(n int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(n)
}

func randomFrom(source []string) string {
	return source[seedAndReturnRandom(len(source))]
}
